﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace UPSSystemCS
{
    public partial class WebForm5 : System.Web.UI.Page
    {
          cUps obj = new cUps();
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                if (!IsPostBack)
                {
                    getItems();
                }
               
            }
            catch (Exception)
            {
            }
        }
        protected void ddlUPSList_SelectedIndexChanged(object sender, EventArgs e)
        {
            viewUpsData();
            viewBatteryData();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            obj.UpsSno=ddlUps.SelectedItem.ToString();
            obj.UPSlocation = ddlLocation.SelectedItem.ToString();
           obj.UpdateUPSData();
        }
        void getItems()
        {
            try
            {
                DataTable dt = obj.getUpsId();
                ddlUps.DataSource = dt;
                     
                ddlUps.DataValueField = "UpsId";
                ddlUps.DataTextField = "serial_no";
                ddlUps.Items.Insert(0, "--Select One--");
                ddlUps.SelectedIndex = 0;
                ddlUps.DataBind();
            }
            catch (Exception)
            {

                throw;
            }

        }

        void viewUpsData()
        {
            try
            {
                string serialNo = Convert.ToString(ddlUps.SelectedItem);

                DataTable dt = new DataTable();
                dt = obj.getData(serialNo);
                lblCompany.Text = dt.Rows[0]["CompanyName"].ToString();
                lblNoOfBattery.Text = dt.Rows[0]["NoOfBattery"].ToString();
                lblComputerAttach.Text = dt.Rows[0]["ComputerAttachedTo"].ToString();
                lblModel.Text = dt.Rows[0]["Model"].ToString();
                lblVendor.Text = dt.Rows[0]["ManufactureVendor"].ToString();
                lblDateOfPurchase.Text = Convert.ToString(dt.Rows[0]["DateOfPurchase"]).Substring(0, 10);
                lblWarranty.Text = Convert.ToString(dt.Rows[0]["warrantyPeriod"]);
                lblCapacity.Text = Convert.ToString(dt.Rows[0]["Capacity"]);
                ddlLocation.Text = Convert.ToString(dt.Rows[0]["UpsLocation"]);
            }
            catch (Exception)
            {
                
                //throw;
            }
           
        
        }
        void viewBatteryData()
        {
            string serialNo = Convert.ToString(ddlUps.SelectedItem);

            DataTable dt = new DataTable();
            dt = obj.getBatteryData(serialNo);
            
            grdUps.DataSource = dt;
            grdUps.DataBind();

        }

        protected void grdUps_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            string upsNo = ddlUps.SelectedItem.ToString();
            string BattrySrNo =((Label) grdUps.Rows[index].FindControl("lblBatterySrNo")).Text;
            obj.RemoveBattery(BattrySrNo, upsNo);
        }

        protected void grdUps_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {

        }
    }
}
