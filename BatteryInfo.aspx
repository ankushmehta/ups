﻿<%@ Page Language="C#" MasterPageFile="~/UPS.Master" AutoEventWireup="true" CodeBehind="BatteryInfo.aspx.cs" Inherits="UPSSystemCS.WebForm4" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div>
    <asp:GridView ID="grvUps" runat="server" AutoGenerateColumns="False">
            <Columns>
            
                <asp:TemplateField>
               
                    <HeaderTemplate>
                    
                        <asp:Label ID="Label1" runat="server" Text="Battery Sno"></asp:Label>
                        
                    </HeaderTemplate>
                    <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%#Eval("BatterySno")%>'></asp:Label>
                       
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtBatterySrNo" runat="server"></asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label2" runat="server" Text="Model"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                             <asp:Label ID="Label1" runat="server" Text='<%#Eval("Model")%>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtBatterySrNo" Text="" runat="server"></asp:TextBox>
                        </EditItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField>
                      <HeaderTemplate>
                           <asp:Label ID="Label4" runat="server" Text="Company"></asp:Label>
                      </HeaderTemplate>
                         <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%#Eval("Company")%>'></asp:Label>
                       
                         </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtBatterySrNo" runat="server"></asp:TextBox>
                        </EditItemTemplate>
                     </asp:TemplateField>
                  <asp:TemplateField>
                       <HeaderTemplate>
                           <asp:Label ID="Label4" runat="server" Text="Vendor/Manufacturer"></asp:Label>
                      </HeaderTemplate>
                      <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%#Eval("vendorManufacturer")%>'></asp:Label>
                       
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtBatterySrNo" runat="server"></asp:TextBox>
                    </EditItemTemplate>
                      </asp:TemplateField>
                       <asp:TemplateField>
                       <HeaderTemplate>
                           <asp:Label ID="DateOfPurchase" runat="server" Text="Date Of Purchase"></asp:Label>
                      </HeaderTemplate>
                      <ItemTemplate>
                            <asp:Label ID="lblDateOfPurchase" runat="server" Text='<%#Eval("DateOfPurchase")%>'></asp:Label>
                       
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtBatterySrNo" runat="server"></asp:TextBox>
                    </EditItemTemplate>
                      </asp:TemplateField>
                      
                      <asp:TemplateField>
                       <HeaderTemplate>
                           <asp:Label ID="Warrantyperiod" runat="server" Text="Warranty Period"></asp:Label>
                      </HeaderTemplate>
                      <ItemTemplate>
                            <asp:Label ID="lblWarrantyperiod" runat="server" Text='<%#Eval("warrantyPeriod")%>'></asp:Label>
                       
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtWarrantyperiod" runat="server"></asp:TextBox>
                    </EditItemTemplate>
                      </asp:TemplateField>
                      
                        <asp:TemplateField>
                       <HeaderTemplate>
                           <asp:Label ID="Status" runat="server" Text="Status"></asp:Label>
                      </HeaderTemplate>
                      <ItemTemplate>
                            <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status")%>'></asp:Label>
                       
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtStatus" runat="server"></asp:TextBox>
                    </EditItemTemplate>
                      </asp:TemplateField>
                      
                        <asp:TemplateField>
                       <HeaderTemplate>
                           <asp:Label ID="Status" runat="server" Text="Tracing Remarks"></asp:Label>
                      </HeaderTemplate>
                      <ItemTemplate>
                            <asp:Label ID="lblTRemarks" runat="server" Text='<%#Eval("TracingRemarks")%>'></asp:Label>
                       
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtRemarks" runat="server"></asp:TextBox>
                    </EditItemTemplate>
                      </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
</asp:Content>
