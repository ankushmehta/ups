﻿<%@ Page Language="C#" MasterPageFile="~/UPS.Master" AutoEventWireup="true" CodeBehind="UPSReports.aspx.cs" Inherits="UPSSystemCS.WebForm1" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div>
    <table>
    <tr>
    <tr>
            <td>
                UPS Serial Number :
            </td>
  
            <td>
             <asp:DropDownList ID="ddlUps" runat="server" 
                        onselectedindexchanged="ddlUPSList_SelectedIndexChanged" 
                        AutoPostBack="True">
                    </asp:DropDownList>
             </td>
    </tr>  
  </table>
  
    <asp:GridView ID="grdUps" runat="server" AutoGenerateColumns="False" 
                onrowcommand="grdUps_RowCommand">
            <Columns>
            
                <asp:TemplateField>
               
                    <HeaderTemplate>
                    
                        <asp:Label ID="Label1" runat="server" Text="Ups Id"></asp:Label>
                        
                    </HeaderTemplate>
                    <ItemTemplate>
                            <asp:Label ID="lblUpsId" runat="server" Text='<%#Eval("UpsId")%>'></asp:Label>

                    </ItemTemplate>
                    
                </asp:TemplateField>
                <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label2" runat="server" Text="Capacity"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                                <asp:Label ID="lblCapacity" runat="server" Text='<%#Eval("Capacity")%>'></asp:Label>
                        </ItemTemplate>
                       
                </asp:TemplateField>
                 <asp:TemplateField>
                      <HeaderTemplate>
                           <asp:Label ID="Label4" runat="server" Text="Company"></asp:Label>
                      </HeaderTemplate>
                         <ItemTemplate>
                            <asp:Label ID="lblCompany" runat="server" Text='<%#Eval("CompanyName")%>'></asp:Label>

                         </ItemTemplate>
                       
                     </asp:TemplateField>
                  <asp:TemplateField>
                       <HeaderTemplate>
                           <asp:Label ID="Label4" runat="server" Text="Ups Location"></asp:Label>
                      </HeaderTemplate>
                      <ItemTemplate>
                           <asp:Label ID="lblLocation" runat="server" Text='<%#Eval("UpsLocation")%>'></asp:Label>

                    </ItemTemplate>
                    
                      </asp:TemplateField>
                       <asp:TemplateField>
                       <HeaderTemplate>
                           <asp:Label ID="DateOfPurchase" runat="server" Text="No Of Battery"></asp:Label>
                      </HeaderTemplate>
                      <ItemTemplate>
                          <asp:Label ID="lblNoOfBattery" runat="server" Text='<%#Eval("NoOfBattery")%>'></asp:Label>
                         
                    </ItemTemplate>
                    
                      </asp:TemplateField>
                      
                      <asp:TemplateField>
                       <HeaderTemplate>
                           <asp:Label ID="Warrantyperiod" runat="server" Text="Computer Attached To"></asp:Label>
                      </HeaderTemplate>
                      <ItemTemplate>
                          
                            <asp:Label ID="lblComputerAttachedTo" runat="server" Text='<%#Eval("ComputerAttachedTo")%>'></asp:Label>

                    </ItemTemplate>
                       
                    
                    
                      </asp:TemplateField>
                      
                        <asp:TemplateField>
                       <HeaderTemplate>
                           <asp:Label ID="Status" runat="server" Text="Model"></asp:Label>
                      </HeaderTemplate>
                      <ItemTemplate>
                          <asp:Label ID="lblModel" runat="server" Text='<%#Eval("Model")%>'></asp:Label>
                       
                       
                    </ItemTemplate>
                    
                      </asp:TemplateField>
                      
                        <asp:TemplateField>
                       <HeaderTemplate>
                           <asp:Label ID="Tracing" runat="server" Text="Manufacture Vendor"></asp:Label>
                      </HeaderTemplate>
                      <ItemTemplate>
                          <asp:Label ID="lblManufacture" runat="server" Text='<%#Eval("ManufactureVendor")%>'></asp:Label>

                       
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtRemarks" runat="server"></asp:TextBox>
                    </EditItemTemplate>
                      </asp:TemplateField>
                      
                       <asp:TemplateField>
                       <HeaderTemplate>
                           <asp:Label ID="Tracing" runat="server" Text="Serial Number"></asp:Label>
                      </HeaderTemplate>
                      <ItemTemplate>
                          <asp:Label ID="lblSerial" runat="server" Text='<%#Eval("serial_no")%>'></asp:Label>

                       
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtRemarks" runat="server"></asp:TextBox>
                    </EditItemTemplate>
                      </asp:TemplateField>
                      
                      
                       <asp:TemplateField>
                       <HeaderTemplate>
                           <asp:Label ID="Tracing" runat="server" Text="Date Of Purchase"></asp:Label>
                      </HeaderTemplate>
                      <ItemTemplate>
                          <asp:Label ID="lblDateOfPurchase" runat="server" Text='<%#Eval("DateOfPurchase")%>'></asp:Label>

                       
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtRemarks" runat="server"></asp:TextBox>
                    </EditItemTemplate>
                      </asp:TemplateField>
                      
                       <asp:TemplateField>
                       <HeaderTemplate>
                           <asp:Label ID="Tracing" runat="server" Text="Warranty Period"></asp:Label>
                      </HeaderTemplate>
                      <ItemTemplate>
                          <asp:Label ID="lblWarranty" runat="server" Text='<%#Eval("warrantyPeriod")%>'></asp:Label>

                       
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtRemarks" runat="server"></asp:TextBox>
                    </EditItemTemplate>
                      </asp:TemplateField>
                      
                      <asp:TemplateField>
                       <HeaderTemplate>
                           <asp:Label ID="Tracing" runat="server" Text="Is In Warranty"></asp:Label>
                      </HeaderTemplate>
                      <ItemTemplate>
                          <asp:Label ID="lblIsInWarranty" runat="server" Text='<%#Eval("Isinwarranty")%>'></asp:Label>

                       
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtRemarks" runat="server"></asp:TextBox>
                    </EditItemTemplate>
                      </asp:TemplateField>
                    
                      
                <asp:ButtonField Text="Battery Info" />
                    
                      
            </Columns>
        </asp:GridView>
       
    </div>
</asp:Content>
