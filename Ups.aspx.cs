﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace UPSSystemCS
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        cBattery obj = new cBattery();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

        }


        void getData()
        {

            try
            {
                DataTable dt = new DataTable();
                int rows = Convert.ToInt32(ddlNoOfBattery.SelectedItem.ToString());
                for (int i = 0; i < rows; i++)
                {

                    dt.Rows.Add();
                }

                grdUps.DataSource = dt;
                grdUps.DataBind();
            }
            catch (Exception)
            {

                // throw;
            }

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            SaveDataForm();
            SaveDataGrid();
        }


        void SaveDataForm()
        {
            cUps obj1 = new cUps();
            obj1.UpsSno = txtUpsSrNo.Text;
            obj1.Company = txtCompany.Text;
            obj1.DateOfPurchase = txtDateOfPurchase.Text;
            obj1.Model = txtModel.Text;
            obj1.Capacity = txtCapacity.Text;

            obj1.vendorManufacturer = txtVendor.Text;
            obj1.warrantyPeriod = ddlWarrantyP.SelectedItem.ToString();
            obj1.UPSlocation = Convert.ToString(ddlLocation.SelectedItem);
            obj1.NoOfBattery = Convert.ToInt32(ddlNoOfBattery.SelectedItem.ToString());
            obj1.ComputerAtatchedTo = txtComputerAttachedTo.Text;
            if (rbtYes.Checked)
            {
                obj1.Isinwarranty = "yes";
            }
            else
            {
                obj1.Isinwarranty = "no";
            }
            obj1.InsertUPSData();

        }
        void SaveDataGrid()
        {
            for (int i = 0; i < grdUps.Rows.Count; i++)
            {


                obj.BatterySno = ((TextBox)grdUps.Rows[i].FindControl("txtBatterySrNo")).Text;
                obj.Model = ((TextBox)(grdUps.Rows[i].FindControl("txtModel"))).Text;
                obj.Company = ((TextBox)(grdUps.Rows[i].FindControl("txtCompany"))).Text;
                obj.vendorManufacturer = ((TextBox)(grdUps.Rows[i].FindControl("txtVendor"))).Text;
                obj.DateOfPurchase = ((TextBox)(grdUps.Rows[i].FindControl("txtDateOfPurchase"))).Text;
                obj.TracingRemarks = ((TextBox)(grdUps.Rows[i].FindControl("txtTracing"))).Text;

                obj.UId = txtUpsSrNo.Text;
                obj.Status = "Original";
                obj.warrantyPeriod = ((TextBox)(grdUps.Rows[i].FindControl("txtWarrantyperiod"))).Text;
                int j = obj.InsertBatteryData();

                if (j > 0)
                {
                    lblInfo.Text = "Your Data Is Submitted";

                }
            }
        }

        protected void ddlNoOfBattery_SelectedIndexChanged(object sender, EventArgs e)
        {

            getData();

        }
    }
}
