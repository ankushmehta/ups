﻿<%@ Page Language="C#" MasterPageFile="~/UPS.Master" AutoEventWireup="true" CodeBehind="UpsTransaction.aspx.cs" Inherits="UPSSystemCS.WebForm5" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
    <div>
    <table>
            <tr>
                <th colspan="2">
                    UPS Information
                </th>
            </tr> 
            <tr>
                <td>
                    UPS Serial Number
                </td>
                <td style="margin-left: 40px">
                    <asp:DropDownList ID="ddlUps" runat="server" 
                        onselectedindexchanged="ddlUPSList_SelectedIndexChanged" 
                        AutoPostBack="True" AppendDataBoundItems="True">
                    </asp:DropDownList>
                </td>

            </tr>
             <tr>
                <td>
                Company
                </td>
                <td>
                    <asp:Label ID="lblCompany" runat="server" Text="Label"></asp:Label>
                </td>

            </tr>
            <tr>
                <td>
               No Of Batteries.
                </td>
                <td>
                    <asp:Label ID="lblNoOfBattery" runat="server" Text="Label"></asp:Label>
                </td>

            </tr>
             <tr>
                <td>
                UPS Location
                </td>
                <td>
                    <asp:DropDownList ID="ddlLocation" runat="server">
                        <asp:ListItem>SCF</asp:ListItem>
                        <asp:ListItem>JCF</asp:ListItem>
                        <asp:ListItem>541</asp:ListItem>
                        <asp:ListItem>532</asp:ListItem>
                        <asp:ListItem>BPTP</asp:ListItem>
                    </asp:DropDownList>
                   
                </td>

            </tr>
             <tr>
                <td>
                Computer Attached To
                </td>
                <td>
                    <asp:Label ID="lblComputerAttach" runat="server" Text="Label"></asp:Label>
                </td>

            </tr>
             <tr>
                <td>
                Model
                </td>
                <td>
                    <asp:Label ID="lblModel" runat="server" Text="Label"></asp:Label>

                </td>

            </tr>
             <tr>
                <td>
                Vendor/Manufacturer
                </td>
                <td>
                    <asp:Label ID="lblVendor" runat="server" Text="Label"></asp:Label>

                </td>

            </tr>
             <tr>
                <td>
                Date Of Purchase
                </td>
                <td>
                    <asp:Label ID="lblDateOfPurchase" runat="server" Text="Label"></asp:Label>
                </td>

            </tr>
            <tr>
                <td>
                Warranty Period
                </td>
                <td>
                    <asp:Label ID="lblWarranty" runat="server" Text="Label"></asp:Label>
                </td>

            </tr>
            <tr>
                <td>
                    Capacity</td>
                <td>
                    <asp:Label ID="lblCapacity" runat="server" Text="Label"></asp:Label>
                </td>

            </tr>
            <tr>
                <td>
                    IsInWaranty</td>
                <td>
                    <asp:RadioButton ID="rbtYes" runat="server" Text="Yes" GroupName="warranty" />
                    <asp:RadioButton ID="rbtNo"  runat="server" Text="No" GroupName="warranty" />
                </td>

            </tr>
           
    </table>
    </div>
    <div style="display:inline">
        <asp:GridView ID="grdUps" runat="server" AutoGenerateColumns="False" 
            onrowcommand="grdUps_RowCommand" 
            onrowcancelingedit="grdUps_RowCancelingEdit">
            <Columns>
            
                <asp:TemplateField>
               
                    <HeaderTemplate>
                    
                        <asp:Label ID="Label1" runat="server" Text="Battery Sno"></asp:Label>
                        
                    </HeaderTemplate>
                    <ItemTemplate>
                            <asp:Label ID="lblBatterySrNo" runat="server" Text='<%#Eval("BatterySno")%>'></asp:Label>

                    </ItemTemplate>
                    
                </asp:TemplateField>
                <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label2" runat="server" Text="Model"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                                <asp:Label ID="lblModel" runat="server" Text='<%#Eval("Model")%>'></asp:Label>
                        </ItemTemplate>
                       
                </asp:TemplateField>
                 <asp:TemplateField>
                      <HeaderTemplate>
                           <asp:Label ID="Label4" runat="server" Text="Company"></asp:Label>
                      </HeaderTemplate>
                         <ItemTemplate>
                            <asp:Label ID="lblCompany" runat="server" Text='<%#Eval("Model")%>'></asp:Label>

                         </ItemTemplate>
                       
                     </asp:TemplateField>
                  <asp:TemplateField>
                       <HeaderTemplate>
                           <asp:Label ID="Label4" runat="server" Text="Vendor"></asp:Label>
                      </HeaderTemplate>
                      <ItemTemplate>
                           <asp:Label ID="lblVendor" runat="server" Text='<%#Eval("vendorManufacturer")%>'></asp:Label>

                    </ItemTemplate>
                    
                      </asp:TemplateField>
                       <asp:TemplateField>
                       <HeaderTemplate>
                           <asp:Label ID="DateOfPurchase" runat="server" Text="Date Of Purchase"></asp:Label>
                      </HeaderTemplate>
                      <ItemTemplate>
                          <asp:Label ID="lblDateOfPurchase" runat="server" Text='<%#Eval("DateOfPurchase")%>'></asp:Label>
                         
                    </ItemTemplate>
                    
                      </asp:TemplateField>
                      
                      <asp:TemplateField>
                       <HeaderTemplate>
                           <asp:Label ID="Warrantyperiod" runat="server" Text="Warranty Period"></asp:Label>
                      </HeaderTemplate>
                      <ItemTemplate>
                          
                            <asp:Label ID="lblWarranty" runat="server" Text='<%#Eval("warrantyPeriod")%>'></asp:Label>

                    </ItemTemplate>
                       
                    
                    
                      </asp:TemplateField>
                      
                        <asp:TemplateField>
                       <HeaderTemplate>
                           <asp:Label ID="Status" runat="server" Text="Status"></asp:Label>
                      </HeaderTemplate>
                      <ItemTemplate>
                          <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status")%>'></asp:Label>
                       
                       
                    </ItemTemplate>
                    
                      </asp:TemplateField>
                      
                        <asp:TemplateField>
                       <HeaderTemplate>
                           <asp:Label ID="Tracing" runat="server" Text="Tracing Remarks"></asp:Label>
                      </HeaderTemplate>
                      <ItemTemplate>
                          <asp:Label ID="lblTracing" runat="server" Text='<%#Eval("TracingRemarks")%>'></asp:Label>

                       
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtRemarks" runat="server"></asp:TextBox>
                    </EditItemTemplate>
                      </asp:TemplateField>
                <asp:ButtonField CommandName="Cancel" Text="Remove Battery" />
            </Columns>
        </asp:GridView>
    <div  style="width:100%"  align="center"> <asp:Button ID="btnSubmit" runat="server" Text="Submit" onclick="btnSubmit_Click" /></div>
    </div>
       
    
    
    </div>
</asp:Content>
