﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace UPSSystemCS
{
    public partial class WebForm4 : System.Web.UI.Page
    {
         cUps obj = new cUps();
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                string srNo = Request.QueryString["Param"].ToString();

                if (!IsPostBack)
                {
                    viewBatteryData(srNo);
                }

            }
            catch (Exception)
            {


            }
            
        }

        void viewBatteryData(string serialNo)
        {
 //           string serialNo = Convert.ToString(ddlUps.SelectedItem);

            DataTable dt = new DataTable();
            dt = obj.getBatteryData(serialNo);
            grvUps.DataSource = dt;
            grvUps.DataBind();

        }

    }
    }

