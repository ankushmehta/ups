﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace UPSSystemCS
{
    public partial class WebForm3 : System.Web.UI.Page
    {
        cBattery obj = new cBattery();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                getItems();
                getData();

            }
            catch (Exception)
            {


            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            obj.BatterySno = txtBatterySrNo.Text;
            obj.Company = txtCompany.Text;
            obj.DateOfPurchase = txtDate.Text;
            obj.Model = txtModel.Text;
            obj.Status = ddlStatus.SelectedItem.ToString();
            obj.TracingRemarks = txtTracingRemarks.Text;
            obj.UId = Convert.ToString(ddlUps.SelectedValue);
            obj.vendorManufacturer = txtVendor.Text;
            // obj.warrantyPeriod = txtWarrantyPeriod.Text;
            obj.InsertBatteryData();
        }
        void getItems()
        {
            try
            {
                DataTable dt = obj.getUpsId();
                ddlUps.DataSource = dt;
                ddlUps.DataBind();

                ddlUps.DataValueField = "UpsId";
                ddlUps.DataTextField = "serial_no";
                ddlUps.DataBind();
            }
            catch (Exception)
            {

                throw;
            }

        }
        void getData()
        {
            try
            {
                DataTable dt = obj.getData();


                grvUps.DataSource = dt;
                grvUps.DataBind();
            }
            catch (Exception)
            {

                throw;
            }

        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
