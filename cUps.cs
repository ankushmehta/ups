﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

namespace UPSSystemCS
{



    public class cUps
    {
        SqlConnection cn = new SqlConnection("server=ONL-10;database=UpsManagementSystem;Integrated Security=true;");
             
        public string UpsSno { get; set; }
        public string Capacity { get; set; }
        public string Company { get; set; }
        public string Model { get; set; }
        public string vendorManufacturer { get; set; }
        public string DateOfPurchase { get; set; }
        public string warrantyPeriod { get; set; }
        public string Isinwarranty { get; set; }
        public string UId { get; set; }
        public int NoOfBattery { get; set; }
        public string UPSlocation { get; set; }
        public string ComputerAtatchedTo { get; set; }

        public void InsertUPSData()
        {
            try
            {

                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spInsertUps";
                cmd.Connection = cn;
                cn.Open();

                cmd.Parameters.Add("@pUpsSno", SqlDbType.VarChar);
                cmd.Parameters["@pUpsSno"].Value = this.UpsSno;

                cmd.Parameters.Add("@pCapacity", SqlDbType.VarChar);
                cmd.Parameters["@pCapacity"].Value = this.Capacity;

                cmd.Parameters.Add("@pCompany", SqlDbType.VarChar);
                cmd.Parameters["@pCompany"].Value = this.Company;

                cmd.Parameters.Add("@pModel", SqlDbType.VarChar);
                cmd.Parameters["@pModel"].Value = this.Model;

                cmd.Parameters.Add("@pvendorManufacturer", SqlDbType.VarChar);
                cmd.Parameters["@pvendorManufacturer"].Value = this.vendorManufacturer;


                cmd.Parameters.Add("@pDateOfPurchase", SqlDbType.VarChar);
                cmd.Parameters["@pDateOfPurchase"].Value = this.DateOfPurchase;

                cmd.Parameters.Add("@pwarrantyPeriod", SqlDbType.VarChar);
                cmd.Parameters["@pwarrantyPeriod"].Value = this.warrantyPeriod;

                cmd.Parameters.Add("@pIsinwarranty", SqlDbType.VarChar);
                cmd.Parameters["@pIsinwarranty"].Value = this.Isinwarranty;

                cmd.Parameters.Add("@pUpsLocation", SqlDbType.VarChar);
                cmd.Parameters["@pUpsLocation"].Value = this.UPSlocation;

                cmd.Parameters.Add("@pNoOfBattery", SqlDbType.Int);
                cmd.Parameters["@pNoOfBattery"].Value = this.NoOfBattery;

                cmd.Parameters.Add("@pComputerAttachedTo", SqlDbType.VarChar);
                cmd.Parameters["@pComputerAttachedTo"].Value = this.ComputerAtatchedTo; 

               
                cmd.ExecuteNonQuery();

            }
            catch (Exception)
            {


            }

        }
        public DataTable getData(string SerialNo)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT UpsId,Capacity,CompanyName,UpsLocation,NoOfBattery,ComputerAttachedTo,Model,ManufactureVendor,serial_no,DateOfPurchase,warrantyPeriod,Isinwarranty FROM UPSMaster where serial_no='"+SerialNo+"'";
            cmd.Connection = cn;
            cn.Open();


            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            ad.Fill(dt);
            cn.Close();
            return dt;
        }

        public DataTable getBatteryData(string SerialNo)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT BatteryId,BatterySno,Company,Model,vendorManufacturer,DateOfPurchase,warrantyPeriod,Status,TracingRemarks,UId FROM UpsManagementSystem.dbo.BatteryMaster where UId='" + SerialNo + "' and isnull(RemovalStatus,'a') !='Removed'";
            cmd.Connection = cn;
            cn.Open();


            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            ad.Fill(dt);
            cn.Close();
            return dt;
        }

        public DataTable getUpsData(string SerialNo)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT UpsId,Capacity,CompanyName,UpsLocation,NoOfBattery,ComputerAttachedTo,Model,ManufactureVendor,serial_no,DateOfPurchase,warrantyPeriod,Isinwarranty FROM UPSMaster where serial_no='" + SerialNo + "'";
            cmd.Connection = cn;
            cn.Open();
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            ad.Fill(dt);
            cn.Close();
            return dt;
        }

        public DataTable getUpsId()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Select UpsId,serial_no from UPSMaster";
            cmd.Connection = cn;
            cn.Open();
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            ad.Fill(dt);
            cn.Close();
            return dt;
        }
        public DataTable getUpsData1()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT UpsId,Capacity,CompanyName,UpsLocation,NoOfBattery,ComputerAttachedTo,Model,ManufactureVendor,serial_no,DateOfPurchase,warrantyPeriod,Isinwarranty FROM UPSMaster";
            cmd.Connection = cn;
            cn.Open();
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            ad.Fill(dt);
            cn.Close();
            return dt;
        }
      
        
        public void   UpdateUPSData()
        {
            try
            {

                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "UPDATE UPSMaster  SET UpsLocation =@pUpsLocation   WHERE serial_no=@pUpsSno";
                cmd.Connection = cn;
                cn.Open();

                cmd.Parameters.Add("@pUpsSno", SqlDbType.VarChar);
                cmd.Parameters["@pUpsSno"].Value = this.UpsSno;

                cmd.Parameters.Add("@pUpsLocation", SqlDbType.VarChar);
                cmd.Parameters["@pUpsLocation"].Value = this.UPSlocation;

               
                cmd.ExecuteNonQuery();

            }
            catch (Exception)
            {


            }

        }

        public void RemoveBattery(string BatteryId,string UpsId)
        {
            try
            {

                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "UPDATE BatteryMaster  SET RemovalStatus ='Removed'   WHERE UId='"+UpsId+"' and BatterySno='"+BatteryId+"'";
                cmd.Connection = cn;
                cn.Open();

              

                cmd.ExecuteNonQuery();

            }
            catch (Exception)
            {


            }

        }

    }
}
