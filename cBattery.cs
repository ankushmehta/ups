﻿using System;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

namespace UPSSystemCS
{
    public class cBattery
    {
        SqlConnection cn = new SqlConnection("server=ONL-10;database=UpsManagementSystem;Integrated Security=true;");
     
        public string BatterySno { get; set; }
        public string Company { get; set; }
        public string Model { get; set; }
        public string vendorManufacturer { get; set; }
        public string DateOfPurchase { get; set; }
        public string warrantyPeriod { get; set; }
        public string Status { get; set; }
        public string TracingRemarks { get; set; }
        public string UId { get; set; }


        public int InsertBatteryData()
        {
            try
            {
             
           SqlCommand cmd=new SqlCommand();
           cmd.CommandType=CommandType.StoredProcedure;
           cmd.CommandText="spInsertBattery";
           cmd.Connection = cn;
           cn.Open();
            
               cmd.Parameters.Add("@pBatterySno", SqlDbType.VarChar);
               cmd.Parameters["@pBatterySno"].Value = this.BatterySno;

               cmd.Parameters.Add("@pCompany", SqlDbType.VarChar);
               cmd.Parameters["@pCompany"].Value = this.Company;

               cmd.Parameters.Add("@pvendorManufacturer", SqlDbType.VarChar);
               cmd.Parameters["@pvendorManufacturer"].Value = this.vendorManufacturer;

               cmd.Parameters.Add("@pModel", SqlDbType.VarChar);
               cmd.Parameters["@pModel"].Value = this.Model;


               cmd.Parameters.Add("@pDateOfPurchase", SqlDbType.VarChar);
               cmd.Parameters["@pDateOfPurchase"].Value = this.DateOfPurchase;

               cmd.Parameters.Add("@pwarrantyPeriod", SqlDbType.VarChar);
               cmd.Parameters["@pwarrantyPeriod"].Value = this.warrantyPeriod;

               cmd.Parameters.Add("@pStatus", SqlDbType.VarChar);
               cmd.Parameters["@pStatus"].Value = this.Status;

               cmd.Parameters.Add("@pTracingRemarks", SqlDbType.VarChar);
               cmd.Parameters["@pTracingRemarks"].Value = this.TracingRemarks;

               cmd.Parameters.Add("@pUId", SqlDbType.VarChar);
               cmd.Parameters["@pUId"].Value = this.UId;

             int i=  cmd.ExecuteNonQuery();
               cn.Close();
               return i;

           }
           catch (Exception)
           {

               return 0; 
           }
          
        }

       public  DataTable getUpsId()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Select UpsId,serial_no from UPSMaster";
            cmd.Connection = cn;
            cn.Open();
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            ad.Fill(dt);
            cn.Close();
            return dt;
        }

       public DataTable getData()
       {
           DataTable dt = new DataTable();
           SqlCommand cmd = new SqlCommand();
           cmd.CommandType = CommandType.Text;
           cmd.CommandText = "SELECT BatteryId,BatterySno,Company,Model,vendorManufacturer,DateOfPurchase,warrantyPeriod,Status,TracingRemarks,UId FROM BatteryMaster";
           cmd.Connection = cn;
           cn.Open();


           SqlDataAdapter ad = new SqlDataAdapter(cmd);
           ad.Fill(dt);
           cn.Close();
           return dt;
       }

    }
}
