﻿<%@ Page Language="C#" MasterPageFile="~/UPS.Master" AutoEventWireup="true" CodeBehind="Battery.aspx.cs" Inherits="UPSSystemCS.WebForm3" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <div>
    <table>
     <tr>
                <td>
               Status
                </td>
                <td>
                    <asp:DropDownList ID="ddlStatus" runat="server" 
                        onselectedindexchanged="DropDownList1_SelectedIndexChanged">
                        <asp:ListItem>Old
                        </asp:ListItem>
                        <asp:ListItem>New</asp:ListItem>
                        <asp:ListItem>InStock</asp:ListItem>
                    </asp:DropDownList>
                </td>

            </tr>
             <tr>
                <td>
                UPS ID
                </td>
                <td>
                    <asp:DropDownList ID="ddlUps" runat="server">
                    
                    </asp:DropDownList>
                </td>

            </tr>
            <tr>
                <td>
                Battery Serial Number
                </td>
                <td>
                    <asp:TextBox ID="txtBatterySrNo" runat="server"></asp:TextBox>
                </td>

            </tr>
             <tr>
                <td>
                Company
                </td>
                <td>
                    <asp:TextBox ID="txtCompany" runat="server"></asp:TextBox>
                </td>

            </tr>
             <tr>
                <td>
                Model
                </td>
                <td>
                 <asp:TextBox ID="txtModel" runat="server"></asp:TextBox>

                </td>

            </tr>
             <tr>
                <td>
                Vendor/Manufacturer</td>
                <td>
                 <asp:TextBox ID="txtVendor" runat="server"></asp:TextBox>

                </td>

            </tr>
             <tr>
                <td>
                Date Of Purchase</td>
                <td>
                    <asp:CalendarExtender ID="CalendarExtender1"  TargetControlID="txtDate" runat="server">
                    </asp:CalendarExtender>
                    <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                    </td>

            </tr>
            <tr>
                <td>
                Warranty Period</td>
                <td>
                    &nbsp;<asp:DropDownList ID="ddlStatus0" runat="server" 
                        onselectedindexchanged="DropDownList1_SelectedIndexChanged">
                        <asp:ListItem>0.5</asp:ListItem>
                        <asp:ListItem>1.0</asp:ListItem>
                        <asp:ListItem>1.5</asp:ListItem>
                        <asp:ListItem>2.0</asp:ListItem>
                        <asp:ListItem>2.5</asp:ListItem>
                        <asp:ListItem>3.0</asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;Year</td>

            </tr>
           
            <tr>
                <td>
               Tracing Remarks</td>
                <td>
                    <asp:TextBox ID="txtTracingRemarks" runat="server"></asp:TextBox>
                </td>

            </tr>
            <tr>
                <td>
               
                </td>
                <td>
                    <asp:Button ID="btnSave" runat="server" Text="Submit" onclick="btnSave_Click" />
                </td>

            </tr>
    </table>
    <div>
        <asp:GridView ID="grvUps" runat="server" AutoGenerateColumns="False">
            <Columns>
            
                <asp:TemplateField>
               
                    <HeaderTemplate>
                    
                        <asp:Label ID="Label1" runat="server" Text="Battery Sno"></asp:Label>
                        
                    </HeaderTemplate>
                    <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%#Eval("BatterySno")%>'></asp:Label>
                       
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtBatterySrNo" runat="server"></asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label2" runat="server" Text="Model"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                             <asp:Label ID="Label1" runat="server" Text='<%#Eval("Model")%>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtBatterySrNo" Text="" runat="server"></asp:TextBox>
                        </EditItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField>
                      <HeaderTemplate>
                           <asp:Label ID="Label4" runat="server" Text="Company"></asp:Label>
                      </HeaderTemplate>
                         <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%#Eval("Company")%>'></asp:Label>
                       
                         </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtBatterySrNo" runat="server"></asp:TextBox>
                        </EditItemTemplate>
                     </asp:TemplateField>
                  <asp:TemplateField>
                       <HeaderTemplate>
                           <asp:Label ID="Label4" runat="server" Text="Vendor/Manufacturer"></asp:Label>
                      </HeaderTemplate>
                      <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%#Eval("vendorManufacturer")%>'></asp:Label>
                       
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtBatterySrNo" runat="server"></asp:TextBox>
                    </EditItemTemplate>
                      </asp:TemplateField>
                       <asp:TemplateField>
                       <HeaderTemplate>
                       
                           <asp:Label ID="DateOfPurchase" runat="server" Text="Date Of Purchase"></asp:Label>
                      </HeaderTemplate>
                      <ItemTemplate>
                            <asp:Label ID="lblDateOfPurchase" runat="server" Text='<%#Eval("DateOfPurchase")%>'></asp:Label>
                       
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtBatterySrNo" runat="server"></asp:TextBox>
                    </EditItemTemplate>
                      </asp:TemplateField>
                      
                      <asp:TemplateField>
                       <HeaderTemplate>
                           <asp:Label ID="Warrantyperiod" runat="server" Text="Warranty Period"></asp:Label>
                      </HeaderTemplate>
                      <ItemTemplate>
                            <asp:Label ID="lblWarrantyperiod" runat="server" Text='<%#Eval("warrantyPeriod")%>'></asp:Label>
                       
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtWarrantyperiod" runat="server"></asp:TextBox>
                    </EditItemTemplate>
                      </asp:TemplateField>
                      
                        <asp:TemplateField>
                       <HeaderTemplate>
                           <asp:Label ID="Status" runat="server" Text="Status"></asp:Label>
                      </HeaderTemplate>
                      <ItemTemplate>
                            <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status")%>'></asp:Label>
                       
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtStatus" runat="server"></asp:TextBox>
                    </EditItemTemplate>
                      </asp:TemplateField>
                      
                        <asp:TemplateField>
                       <HeaderTemplate>
                           <asp:Label ID="Status" runat="server" Text="Tracing Remarks"></asp:Label>
                      </HeaderTemplate>
                      <ItemTemplate>
                            <asp:Label ID="lblTRemarks" runat="server" Text='<%#Eval("TracingRemarks")%>'></asp:Label>
                       
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtRemarks" runat="server"></asp:TextBox>
                    </EditItemTemplate>
                      </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
    </div>
</asp:Content>
