﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace UPSSystemCS
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        cUps obj = new cUps();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    getItems();
                    DataTable dt = obj.getUpsData1();
                    grdUps.DataSource = dt;
                    grdUps.DataBind();
                }

            }
            catch (Exception)
            {


            }
        }

        protected void ddlUPSList_SelectedIndexChanged(object sender, EventArgs e)
        {

            viewBatteryData();
        }

        void getItems()
        {
            try
            {
                DataTable dt = obj.getUpsId();
                ddlUps.DataSource = dt;
                ddlUps.DataBind();

                ddlUps.DataValueField = "UpsId";
                ddlUps.DataTextField = "serial_no";
                ddlUps.DataBind();
            }
            catch (Exception)
            {

                throw;
            }

        }


        void viewBatteryData()
        {
            string serialNo = Convert.ToString(ddlUps.SelectedItem);

            DataTable dt = new DataTable();
            dt = obj.getUpsData(serialNo);
            grdUps.DataSource = dt;
            grdUps.DataBind();

        }

        protected void grdUps_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int index = Convert.ToInt32(e.CommandArgument);
                string serialNo = ((Label)grdUps.Rows[index].FindControl("lblSerial")).Text;
                ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('BatteryInfo.aspx?Param=" + serialNo + "');", true);
            }
            catch (Exception)
            {

                // throw;
            }


        }
    }
}
