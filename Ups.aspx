﻿<%@ Page Language="C#" MasterPageFile="~/UPS.Master" AutoEventWireup="true" CodeBehind="WebForm2.aspx.cs" Inherits="UPSSystemCS.WebForm2" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <div>
    <div>
        
        <table>
            <tr>
                <th colspan="2">
                    UPS Information
                </th>
            </tr> 
            <tr>
                <td>
                    UPS Serial Number
                </td>
                <td>
                    <asp:TextBox ID="txtUpsSrNo" runat="server"></asp:TextBox>
                </td>

            </tr>
             <tr>
                <td>
                Company
                </td>
                <td>
                    <asp:TextBox ID="txtCompany" runat="server"></asp:TextBox>
                </td>

            </tr>
            <tr>
                <td>
               No Of Batteries.
                </td>
                <td>
                    <asp:DropDownList ID="ddlNoOfBattery" runat="server" AutoPostBack="True" 
                        onselectedindexchanged="ddlNoOfBattery_SelectedIndexChanged">
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                    </asp:DropDownList>
                </td>

            </tr>
             <tr>
                <td>
                UPS Location
                </td>
                <td>
                    <asp:DropDownList ID="ddlLocation" runat="server">
                        <asp:ListItem>SCF</asp:ListItem>
                        <asp:ListItem>JCF</asp:ListItem>
                        <asp:ListItem>541</asp:ListItem>
                        <asp:ListItem>532</asp:ListItem>
                        <asp:ListItem>BPTP</asp:ListItem>
                    </asp:DropDownList>
                   
                </td>

            </tr>
             <tr>
                <td>
                Computer Attached To
                </td>
                <td>
                    <asp:TextBox ID="txtComputerAttachedTo" runat="server"></asp:TextBox>
                </td>

            </tr>
             <tr>
                <td>
                Model
                </td>
                <td>
                 <asp:TextBox ID="txtModel" runat="server"></asp:TextBox>

                </td>

            </tr>
             <tr>
                <td>
                Vendor/Manufacturer
                </td>
                <td>
                 <asp:TextBox ID="txtVendor" runat="server"></asp:TextBox>

                </td>

            </tr>
             <tr>
                <td>
                Date Of Purchase
                </td>
                <td>
                    <asp:CalendarExtender ID="CalendarExtender1" TargetControlID="txtDateOfPurchase" runat="server">
                    </asp:CalendarExtender>
                    <asp:TextBox ID="txtDateOfPurchase" runat="server"></asp:TextBox>
                </td>

            </tr>
            <tr>
                <td>
                Warranty Period</td>
                <td>
                    <asp:DropDownList ID="ddlWarrantyP" runat="server">
                        <asp:ListItem Selected="True">0.5</asp:ListItem>
                        <asp:ListItem>1.0</asp:ListItem>
                        <asp:ListItem>1.5</asp:ListItem>
                        <asp:ListItem>2.0</asp:ListItem>
                        <asp:ListItem>2.5</asp:ListItem>
                        <asp:ListItem>3.0</asp:ListItem>
                        <asp:ListItem>3.5</asp:ListItem>
                        <asp:ListItem>4.0</asp:ListItem>
                        <asp:ListItem>4.5</asp:ListItem>
                        <asp:ListItem>5.0</asp:ListItem>
                    </asp:DropDownList>
                    <b>&nbsp;Year</b></td>

            </tr>
            <tr>
                <td>
                    Capacity</td>
                <td>
                    <asp:TextBox ID="txtCapacity" runat="server"></asp:TextBox>
                </td>

            </tr>
            <tr>
                <td>
                    IsInWaranty</td>
                <td>
                    <asp:RadioButton ID="rbtYes" runat="server" Text="Yes" GroupName="warranty" />
                    <asp:RadioButton ID="rbtNo"  runat="server" Text="No" GroupName="warranty" />
                </td>

            </tr>
           
    </table>
    </div>
    <div style="display:inline">
        <asp:GridView ID="grdUps" runat="server" AutoGenerateColumns="False">
            <Columns>
            
                <asp:TemplateField>
               
                    <HeaderTemplate>
                    
                        <asp:Label ID="Label1" runat="server" Text="Battery Sno"></asp:Label>
                        
                    </HeaderTemplate>
                    <ItemTemplate>
                             <asp:TextBox ID="txtBatterySrNo"  runat="server"></asp:TextBox>
                       
                    </ItemTemplate>
                    
                </asp:TemplateField>
                <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label2" runat="server" Text="Model"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                              <asp:TextBox ID="txtModel" Width="80px" Text="" runat="server"></asp:TextBox>
                        </ItemTemplate>
                       
                </asp:TemplateField>
                 <asp:TemplateField>
                      <HeaderTemplate>
                           <asp:Label ID="Label4" runat="server" Text="Company"></asp:Label>
                      </HeaderTemplate>
                         <ItemTemplate>
                             <asp:TextBox ID="txtCompany" Width="80px" runat="server"></asp:TextBox>
                       
                         </ItemTemplate>
                       
                     </asp:TemplateField>
                  <asp:TemplateField>
                       <HeaderTemplate>
                           <asp:Label ID="Label4" runat="server" Text="Vendor"></asp:Label>
                      </HeaderTemplate>
                      <ItemTemplate>
                           <asp:TextBox ID="txtVendor" Width="100px" runat="server"></asp:TextBox>
                       
                    </ItemTemplate>
                    
                      </asp:TemplateField>
                       <asp:TemplateField>
                       <HeaderTemplate>
                           <asp:Label ID="DateOfPurchase" runat="server" Text="Date Of Purchase"></asp:Label>
                      </HeaderTemplate>
                      <ItemTemplate>
                          <asp:CalendarExtender TargetControlID="txtDateOfPurchase" ID="CalendarExtender2" runat="server">
                          </asp:CalendarExtender>
                           <asp:TextBox ID="txtDateOfPurchase" Width="120px" runat="server"></asp:TextBox>
                          
                    </ItemTemplate>
                    
                      </asp:TemplateField>
                      
                      <asp:TemplateField>
                       <HeaderTemplate>
                           <asp:Label ID="Warrantyperiod" runat="server" Text="Warranty Period"></asp:Label>
                      </HeaderTemplate>
                      <ItemTemplate>
                          
                             <asp:TextBox ID="txtWarrantyperiod" Width="120px" runat="server"></asp:TextBox>
                       
                    </ItemTemplate>
                       
                    
                    
                      </asp:TemplateField>
                      
                        <asp:TemplateField>
                       <HeaderTemplate>
                           <asp:Label ID="Status" runat="server" Text="Status"></asp:Label>
                      </HeaderTemplate>
                      <ItemTemplate>
                          <asp:Label ID="Status" runat="server" Text="Original"></asp:Label>
                       
                       
                    </ItemTemplate>
                    
                      </asp:TemplateField>
                      
                        <asp:TemplateField>
                       <HeaderTemplate>
                           <asp:Label ID="Tracing" runat="server" Text="Tracing Remarks"></asp:Label>
                      </HeaderTemplate>
                      <ItemTemplate>
                            <asp:TextBox ID="txtTracing" runat="server"></asp:TextBox>
                       
                       
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtRemarks" runat="server"></asp:TextBox>
                    </EditItemTemplate>
                      </asp:TemplateField>
            </Columns>
        </asp:GridView>
    <div  style="width:90%"  align="center"> <asp:Button ID="btnSubmit" runat="server"  Text="Submit" onclick="btnSubmit_Click" />
        <asp:Label ID="lblInfo" runat="server" ForeColor="Red"></asp:Label>
        </div>    </div>
       
    
    
    </div>
</asp:Content>
